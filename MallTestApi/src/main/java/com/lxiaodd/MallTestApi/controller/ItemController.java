package com.lxiaodd.MallTestApi.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RequestMapping("item")
@RestController
public class ItemController extends BaseApiController{

	/**
	 * 获取类型
	 * @return
	 */
	@RequestMapping("type")
	public String getItemType(){
		return super.getDataByFile("itemtype.json");
	}
	
	@RequestMapping("detail2")
	public List<Map<String,Object>> detail(String typeid) throws JsonParseException, JsonMappingException, IOException {
		List<Map<String,Object>> ret = null;
		List<Map<String,Object>> list = super.getListMapByFile("itemdetail.json");  
		if("1".equals(typeid) ||"2".equals(typeid)||"3".equals(typeid)) {
			ret =super.getlistById(typeid, list, "typeid");
		}else {
			ret = list;
		}
		return ret;
	}
	
	@RequestMapping("detail")
	public Map<String,Object> detail2(String typeid) throws JsonParseException, JsonMappingException, IOException {
		Map<String,Object> ret = new HashMap<>();
		List<String> typeList = new ArrayList<>();
		Map<String,List<Map<String,Object> >> groupData = new HashMap<>();
		ret.put("typeArr", typeList);
		ret.put("dataDic", groupData);
		
		Map<String,Object> map = super.getMapByFile("itemdetail.json");  
		if("1".equals(typeid) ||"2".equals(typeid)||"3".equals(typeid)) {
			Map<String,Object> data = (Map<String,Object>)map.get(typeid);
			String name = data.get("name").toString();
			typeList.add(name);
			groupData.put(name, (List<Map<String,Object> >)data.get("datas"));
		}else {
			for (Entry<String, Object> entry : map.entrySet()) {
				Map<String,Object> data = (Map<String,Object>)entry.getValue();
				String name = data.get("name").toString();
				typeList.add(name);
				groupData.put(name, (List<Map<String,Object> >)data.get("datas"));
			}
		}
		return ret;
	}
	

}
