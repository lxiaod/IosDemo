//
//  AppDelegate.h
//  MallTest2
//
//  Created by peng liu on 2018/9/1.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

