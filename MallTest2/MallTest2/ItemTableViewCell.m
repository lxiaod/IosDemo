//
//  ItemTableViewCell.m
//  MallTest2
//
//  Created by peng liu on 2018/9/2.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import "ItemTableViewCell.h"

@implementation ItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    [self changeSelectionColorForSelectedOrHiglightedState:selected];
}
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    [self changeSelectionColorForSelectedOrHiglightedState:highlighted];
}

- (void)changeSelectionColorForSelectedOrHiglightedState:(BOOL)state
{
    if (state) {
        //选中时候的样式
        self.lbflag.enabled = true;
        self.lbflag.backgroundColor =[UIColor redColor];
        self.lable.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        UIView *backgroundViews = [[UIView alloc]initWithFrame:self.frame];
        backgroundViews.backgroundColor = [UIColor whiteColor];
        [self setSelectedBackgroundView:backgroundViews];
    }else{
        self.lbflag.enabled = false;
        self.lable.font = [UIFont fontWithName:@"System"size:16];
    }
}

@end
