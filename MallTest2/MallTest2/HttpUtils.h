//
//  HttpUtils.h
//  MallTest2
//
//  Created by peng liu on 2018/9/2.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import <Foundation/Foundation.h>
#define URLBasePath @"http://localhost:8080/"

@interface HttpUtils : NSObject
+(void)getDataList:(NSString *)url success:(void(^)(NSData *)) success;
@end
