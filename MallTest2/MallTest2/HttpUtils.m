//
//  HttpUtils.m
//  MallTest2
//
//  Created by peng liu on 2018/9/2.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import "HttpUtils.h"

@implementation HttpUtils
+(void)getDataList:(NSString *)url success:(void(^)(NSData *)) success{
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",URLBasePath,url];
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setHTTPMethod:@"GET"];
    [request setHTTPBody:nil];
    //block方式
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask * task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        success(data);
    }];
    //手动开启
    [task resume];
    }
@end
