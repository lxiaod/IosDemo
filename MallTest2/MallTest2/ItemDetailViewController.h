//
//  ItemDetailViewController.h
//  MallTest2
//
//  Created by peng liu on 2018/9/2.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemDetailViewController : UIViewController
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *idnum;
@end
