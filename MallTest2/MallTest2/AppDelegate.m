//
//  AppDelegate.m
//  MallTest2
//
//  Created by peng liu on 2018/9/1.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

// Not Running(非运行状态)。应用没有运行或被系统终止。
// Inactive(前台非活动状态)。应用正在进入前台状态，但是还不能接受事件处理。
// Active(前台活动状态)。应用进入前台状态，能接受事件处理。
// Background(后台状态)。应用进入后台后，依然能够执行代码。如果有可执行的代码，就会执行代码，
//如果没有可执行的代码或者将可执行的代码执行完毕，应用会马上进入挂起状态。
// Suspended(挂起状态)。处于挂起的应用进入一种“冷冻”状态，不能执行代码。如果系统内存不够，
//应用会被终止。

@implementation AppDelegate

//用户点击应用图标的时候
//在Not running→Inactive阶段。调用application:didFinishLaunchingWithOptions:方法
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

// 在Inactive→Active阶段。调用applicationDidBecomeActive:
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
