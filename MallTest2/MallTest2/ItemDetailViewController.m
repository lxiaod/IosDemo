//
//  ItemDetailViewController.m
//  MallTest2
//
//  Created by peng liu on 2018/9/2.
//  Copyright © 2018年 lxiaod. All rights reserved.
//

#import "ItemDetailViewController.h"

@interface ItemDetailViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labName;
@property (weak, nonatomic) IBOutlet UILabel *labId;
@end

@implementation ItemDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.labName.text = self.name;
    self.labId.text =self.idnum;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
